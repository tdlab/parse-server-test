import 'package:flutter/material.dart';
import 'package:flutterparsetest/home.dart';
import 'package:flutterparsetest/constants.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Parse().initialize(
    keyParseApplicationId,
    keyParseServerUrl,
    masterKey: keyParseMasterKey,
    autoSendSessionId: true,
    debug: true,
    coreStore: await CoreStoreSharedPrefsImp.getInstance(),
  );
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ParseServer Login demo',
      home: Home(),
    );
  }
}
