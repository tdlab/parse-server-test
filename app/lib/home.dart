import 'package:flutter/material.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    _loadUser();
    super.initState();
  }

  // User Credential
  ParseUser _parseUser;

  void _showDialog(String title, String body) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(body),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _runCloudFunction() async {
    final ParseCloudFunction function = ParseCloudFunction('hello');
    final ParseResponse result =
    await function.executeObjectFunction<ParseObject>();
    if (result.success) {
      if (result.result is ParseObject) {
        final ParseObject parseObject = result.result;
        _showDialog(parseObject.parseClassName, parseObject.get('result'));
      }
    }
  }

  void _loadUser() async {
    ParseUser currentUser = await ParseUser.currentUser();
    if (currentUser == null) {
      return null;
    } else {
      _parseUser = currentUser;
      var response = currentUser.login();
      response.catchError((e) {
        print(e);
      });
    }
  }

  void login(username, pass, email) async {
    var user = ParseUser(username, pass, email);
    var response = await user.login();
    if (response.success) {
      setState(() {
        _parseUser = user;
      });
      _showDialog("Login Success", "User ID: " + user.objectId);
    } else {
      _showDialog("Login Error", response.error.message);
    }
  }

  void signUp(username, pass, email) async {
    var user = ParseUser(username, pass, email);
    var response = await user.create();
    if (response.success) {
      setState(() {
        _parseUser = user;
      });
      _showDialog("Sign Up Success", "User ID: " + user.objectId);
    } else {
      _showDialog("Sign Up Error", response.error.message);
    }
  }

  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Parse Test')),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                TextField(
                  controller: usernameController,
                  decoration: InputDecoration(hintText: 'Username'),
                ),
                TextField(
                  controller: emailController,
                  decoration: InputDecoration(hintText: 'Email'),
                ),
                TextField(
                  controller: passController,
                  decoration: InputDecoration(hintText: 'Password'),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                          child: Text('Login'),
                          onPressed: () {
                            login(usernameController.text, passController.text,
                                emailController.text);
                          },
                        ),
                        RaisedButton(
                          onPressed: () {
                            signUp(usernameController.text, passController.text,
                                emailController.text);
                          },
                          child: Text('Sign Up'),
                        ),
                        RaisedButton(
                          child: Text('Logout'),
                          onPressed: () async {
                            var user = _parseUser;
                            var response = await user.logout();
                            if (response.success) {
                              setState(() {
                                _parseUser = null;
                              });
                              _showDialog("Logout Success", "");
                              usernameController.clear();
                              emailController.clear();
                              passController.clear();
                            } else {
                              _showDialog(
                                  "Logout Error", response.error.message);
                            }
                          },
                        )
                      ]),
                ),
                Text("Username: " +
                    (_parseUser == null ? "" : _parseUser.username)),
                Text("Email: " +
                    (_parseUser == null ? "" : _parseUser.emailAddress)),
                Text("User ID: " +
                    (_parseUser == null ? "" : _parseUser.objectId)),
                RaisedButton(
                  child: Text('Run Cloud Function'),
                  onPressed: () {
                    _runCloudFunction();
                  },
                )
              ],
            ),
          ),
        ));
  }
}
