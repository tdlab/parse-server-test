# Parse Server Test

This is a demo project for setting up a local [Parse Server](https://parseplatform.org/) and using it in a Flutter app.

## Getting Started

### Prerequisites

According to the [Parse doc](https://docs.parseplatform.org/parse-server/guide/)

- Node 8 or newer
- MongoDB version 3.6
- Python 2.x (For Windows users, 2.7.1 is the required version)

Additionally for the Flutter app

- [Flutter SDK](https://flutter.dev/docs/get-started/install)
- An Android / iOS simulator or device

### Installing

#### Clone the Project

```bash
git clone https://carbonyl@bitbucket.org/tdlab/parse-server-test.git

cd parse-server-test
```

#### Install Parse and MondoDB

Install Parse Server in the `./server` directory

```bash
sh <(curl -fsSL https://raw.githubusercontent.com/parse-community/parse-server/master/bootstrap.sh)
```

> Any arbitrary string can be used as `Applicaiton Name`, `Application ID` and `Master Key` during the setup. The later two can also be automatically generated.

Run MongoDB and Parse Server locally

```bash
npm install -g mongodb-runner
mongodb-runner start
npm start
```

The console will print the Server URL when the server has started: 
```
parse-server running on [Server URL]
```

#### Configure Server Credencials

Set the following variables in `./app/lib/constants.dart` to the corresponding values in `./server/config.json`:
```dart
keyParseApplicationId: appId
keyParseMasterKey: masterKey
```

Set the `keyParseServerUrl` in `./app/lib/constants.dart` to the Server URL obtained in the last step

#### Run the Flutter App

In `./app`, install dependencies

```bash
flutter pub get
```

Open a simulator or connect your device and run

```bash
flutter run
```
